package com.viewpager

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var customAdapter: CustomAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        setAdapter()

    }

    private fun init() {

        customAdapter = CustomAdapter(baseContext)

    }

    private fun setAdapter() {

        main_container!!.adapter = customAdapter

    }

}